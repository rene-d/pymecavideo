<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="fr">
<context>
    <name>MontreFilm</name>
    <message>
        <location filename="../../src/cadreur.py" line="214"/>
        <source>Voir la vidéo</source>
        <translation>Voir la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="215"/>
        <source>Ralenti : 1/1</source>
        <translation>Ralenti : 1/1</translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="308"/>
        <source>indéf</source>
        <translation type="obsolete">indéf</translation>
    </message>
    <message>
        <location filename="." line="115414494"/>
        <source>indéf.</source>
        <comment>utf8</comment>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>temps en seconde, positions en mètre</source>
        <translation type="obsolete">temps en seconde, positions en mètre</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>point N°</source>
        <translation type="obsolete">point N°</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Evolution de l&apos;ordonnée du point %1</source>
        <translation type="obsolete">Evolution de l&apos;ordonnée du point %1</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Vous avez atteint la fin de la vidéo</source>
        <translation type="obsolete">Vous avez atteint la fin de la vidéo</translation>
    </message>
    <message>
        <location filename="." line="49"/>
        <source>Quelle est la longueur en mètre de votre étalon sur l&apos;image ?</source>
        <translation type="obsolete">Quelle est la longueur en mètre de votre étalon sur l&apos;image ?</translation>
    </message>
    <message>
        <location filename="." line="49"/>
        <source> Merci d&apos;indiquer une échelle valable</source>
        <translation type="obsolete"> Merci d&apos;indiquer une échelle valable</translation>
    </message>
    <message>
        <location filename="." line="38"/>
        <source>Les données seront perdues</source>
        <translation type="obsolete">Les données seront perdues</translation>
    </message>
    <message>
        <location filename="." line="38"/>
        <source>Votre travail n&apos;a pas été sauvegardé
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Votre travail n&apos;a pas été sauvegardé
Voulez-vous les sauvegarder ?</translation>
    </message>
    <message>
        <location filename="." line="22"/>
        <source>Vous avez atteint le début de la vidéo</source>
        <translation type="obsolete">Vous avez atteint le début de la vidéo</translation>
    </message>
    <message>
        <location filename="." line="14"/>
        <source>Ouvrir une vidéo</source>
        <translation type="obsolete">Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Nom de fichier non conforme</translation>
    </message>
    <message>
        <location filename="." line="115413456"/>
        <source>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <comment>utf8</comment>
        <translation type="obsolete">Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Veuillez choisir une image et définir l&apos;échelle</source>
        <translation type="obsolete">Veuillez choisir une image et définir l&apos;échelle</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Désolé pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="obsolete">Désolé pas de fichier d&apos;aide pour le langage %1.</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>indÃ©f.</source>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="." line="1232"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="obsolete">PyMecaVideo, analyse mécanique des vidéos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="86"/>
        <source>Acquisition des données</source>
        <translation type="obsolete">Acquisition des données</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation>Pointage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="384"/>
        <source>Démarrer</source>
        <translation type="obsolete">Démarrer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="418"/>
        <source>efface la série précédente</source>
        <translation type="obsolete">efface la série précédente</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="455"/>
        <source>rétablit le point suivant</source>
        <translation type="obsolete">rétablit le point suivant</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="535"/>
        <source>Tout réinitialiser</source>
        <translation type="obsolete">Tout réinitialiser</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="551"/>
        <source>Définir l&apos;échelle</source>
        <translation type="obsolete">Définir l&apos;échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="419"/>
        <source>indéf.</source>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="621"/>
        <source>Points à 
 étudier:</source>
        <translation type="obsolete">Points à 
 étudier:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="668"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="obsolete">Ordonnées 
vers le bas</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="58"/>
        <source>Lancer le logiciel
 d&apos;acquisition Vidéo</source>
        <translation type="obsolete">Lancer le logiciel
 d&apos;acquisition Vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="229"/>
        <source>Image n°</source>
        <translation type="obsolete">Image n°</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="937"/>
        <source>Voir la vidéo</source>
        <translation type="obsolete">Voir la vidéo</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="897"/>
        <source>Depuis ce référentiel</source>
        <translation type="obsolete">Depuis ce référentiel</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="809"/>
        <source>Échelle de vitesses</source>
        <translation type="obsolete">Échelle de vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="209"/>
        <source>Acquisition</source>
        <translation type="obsolete">Acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="639"/>
        <source>suivi
automatique</source>
        <translation type="obsolete">suivi
automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation>Changer d&apos;origine</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1966"/>
        <source>Choisir ...</source>
        <translation>Choisir ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="868"/>
        <source>Chrono
photographie</source>
        <translation type="obsolete">Chrono
photographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1438"/>
        <source>Enregistrer</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="972"/>
        <source>Coordonnées</source>
        <translation type="obsolete">Coordonnées</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="978"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="obsolete">Tableau des dates et des coordonnées</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1079"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation>Copier les mesures dans le presse papier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1058"/>
        <source>Exporter vers ....</source>
        <translation type="obsolete">Exporter vers ....</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="997"/>
        <source>Oo.o Calc</source>
        <translation type="obsolete">Oo.o Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1073"/>
        <source>Qtiplot</source>
        <translation type="obsolete">Qtiplot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1078"/>
        <source>SciDAVis</source>
        <translation type="obsolete">SciDAVis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1015"/>
        <source>changer d&apos;échelle ?</source>
        <translation type="obsolete">changer d&apos;échelle ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1513"/>
        <source>&amp;Fichier</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1517"/>
        <source>E&amp;xporter vers ...</source>
        <translation>E&amp;xporter vers ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1624"/>
        <source>&amp;Aide</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1545"/>
        <source>&amp;Edition</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1102"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation type="obsolete">&amp;Ouvrir une vidéo (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1574"/>
        <source>avanceimage</source>
        <translation>avanceimage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1579"/>
        <source>reculeimage</source>
        <translation>reculeimage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1120"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="obsolete">Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1128"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation type="obsolete">Enregistrer les données (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1136"/>
        <source>À &amp;propos</source>
        <translation type="obsolete">À &amp;propos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1141"/>
        <source>Aide (F1)</source>
        <translation type="obsolete">Aide (F1)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1149"/>
        <source>Exemples ...</source>
        <translation type="obsolete">Exemples ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1154"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation type="obsolete">Ouvrir un projet &amp;mecavidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1159"/>
        <source>&amp;Préférences</source>
        <translation type="obsolete">&amp;Préférences</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1531"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="obsolete">&amp;Copier dans le presse-papier (Ctrl-C)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1172"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation type="obsolete">Défaire (Ctrl-Z)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1180"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="obsolete">Refaire (Ctrl-Y)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1188"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="obsolete">OpenOffice.org &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1702"/>
        <source>Qti&amp;plot</source>
        <translation>Qti&amp;plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1707"/>
        <source>Sci&amp;davis</source>
        <translation>Sci&amp;davis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="189"/>
        <source>Pas de vidéos chargées</source>
        <translation type="obsolete">Pas de vidéos chargées</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="280"/>
        <source>Acquisition video</source>
        <translation type="obsolete">Acquisition vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="782"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="obsolete">Montrer 
les vecteurs
vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="791"/>
        <source>près de
la souris</source>
        <translation type="obsolete">près de
la souris</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="806"/>
        <source>partout</source>
        <translation>partout</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="966"/>
        <source>Définir un autre référentiel : </source>
        <translation type="obsolete">Définir un autre référentiel : </translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="314"/>
        <source>Lancer %1
 pour capturer une vidÃ©o</source>
        <translation type="obsolete">Lancer %1
 pour capturer une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>indÃ©f</source>
        <translation>indéf</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="332"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NON DISPO : %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation>Ouvrir un projet Pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1221"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">fichiers pymecavideo(*.csv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en mÃ¨tre</source>
        <translation>temps en seconde, positions en mètre</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1245"/>
        <source>point NÂ° %1</source>
        <translation type="obsolete">point N° %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1452"/>
        <source>Veuillez sÃ©lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arrÃªter Ã&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">Veuillez sélectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arrêter à tous moments la capture en appuyant sur le bouton</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution de l&apos;abscisse du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;ordonnÃ©e du point %1</source>
        <translation type="obsolete">Evolution de l&apos;ordonnée du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution de la vitesse du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1496"/>
        <source>Pointage des positionsÂ&#xa0;: cliquer sur le point NÂ° %1</source>
        <translation type="obsolete">Pointage des positions : cliquer sur le point N° %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vidÃ©o</source>
        <translation>Vous avez atteint la fin de la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2551"/>
        <source>DÃ©finir une Ã©chelle</source>
        <translation type="obsolete">Définir une échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en mÃ¨tre de votre Ã©talon sur l&apos;image ?</source>
        <translation>Quelle est la longueur en mètre de votre étalon sur l&apos;image ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d&apos;indiquer une Ã©chelle valable</source>
        <translation> Merci d&apos;indiquer une échelle valable</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donnÃ©es seront perdues</source>
        <translation>Les données seront perdues</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n&apos;a pas Ã©tÃ© sauvegardÃ©
Voulez-vous les sauvegarder ?</source>
        <translation>Votre travail n&apos;a pas été sauvegardé
Voulez-vous les sauvegarder ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1782"/>
        <source>Vous avez atteint le dÃ©but de la vidÃ©o</source>
        <translation type="obsolete">Vous avez atteint le début de la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2707"/>
        <source>Ouvrir une vidÃ©o</source>
        <translation>Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Nom de fichier non conforme</source>
        <translation>Nom de fichier non conforme</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1862"/>
        <source>Veuillez choisir une image et dÃ©finir l&apos;Ã©chelle</source>
        <translation type="obsolete">Veuillez choisir une image et définir l&apos;échelle</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1907"/>
        <source>DÃ©solÃ© pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="obsolete">Désolé pas de fichier d&apos;aide pour le langage %1.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1798"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="660"/>
        <source>Enregistrer la chronophotographie</source>
        <translation type="obsolete">Enregistrer la chronophotographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation>fichiers images(*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="446"/>
        <source>NON DISPO : {0}</source>
        <translation type="obsolete">NON DISPO : {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1251"/>
        <source>point N° {0}</source>
        <translation type="obsolete">point N° {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de l&apos;abscisse du point {0}</source>
        <translation type="obsolete">Evolution de l&apos;abscisse du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1446"/>
        <source>Evolution de l&apos;ordonnée du point {0}</source>
        <translation type="obsolete">Evolution de l&apos;ordonnée du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de la vitesse du point {0}</source>
        <translation type="obsolete">Evolution de la vitesse du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1494"/>
        <source>Pointage des positions&#xa0;: cliquer sur le point N° {0}</source>
        <translation type="obsolete">Pointage des positions : cliquer sur le point N° {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1901"/>
        <source>Désolé pas de fichier d&apos;aide pour le langage {0}.</source>
        <translation type="obsolete">Désolé pas de fichier d&apos;aide pour le langage {0}.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1898"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1976"/>
        <source>Veuillez choisir une image (et définir l&apos;échelle)</source>
        <translation type="obsolete">Veuillez choisir une image (et définir l&apos;échelle)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2066"/>
        <source>Pymecavideo n&apos;arrive pas à lire l&apos;image</source>
        <translation type="obsolete">Pymecavideo n&apos;arrive pas à lire l&apos;image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="882"/>
        <source>Fichier Python créé</source>
        <translation type="obsolete">Fichier Python créé</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1494"/>
        <source>Pointage Automatique</source>
        <translation>Pointage Automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1196"/>
        <source>le format de cette vidéo n&apos;est pas pris en charge par pymecavideo</source>
        <translation type="obsolete">le format de cette vidéo n&apos;est pas pris en charge par pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1270"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation>Le fichier choisi n&apos;est pas compatible avec pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Masse de l&apos;objet</source>
        <translation>Masse de l&apos;objet</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation>Quelle est la masse de l&apos;objet ? (en kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2352"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation> Merci d&apos;indiquer une masse valable</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>MAUVAISE VALEUR !</source>
        <translation>MAUVAISE VALEUR !</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>La valeur rentrée n&apos;est pas compatible avec le calcul</source>
        <translation>La valeur rentrée n&apos;est pas compatible avec le calcul</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation>px/m</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation>Abscisses 
vers la gauche</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="741"/>
        <source>Trajectoires</source>
        <translation>Trajectoires</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="796"/>
        <source>près de la souris</source>
        <translation>près de la souris</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="836"/>
        <source>px pour 1 m/s</source>
        <translation>px pour 1 m/s</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1333"/>
        <source>9.8</source>
        <translation>9.8</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1280"/>
        <source>Grapheur</source>
        <translation>Grapheur</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1377"/>
        <source>en fonction de </source>
        <translation>en fonction de </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1636"/>
        <source>&amp;Exemples ...</source>
        <translation>&amp;Exemples ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1697"/>
        <source>LibreOffice &amp;Calc</source>
        <translation>LibreOffice &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1712"/>
        <source>&amp;Python (source)</source>
        <translation>&amp;Python (source)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation>IPS :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre d&apos;images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre d&apos;images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation>000</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="558"/>
        <source>Tourner l&apos;image de 90° vers la gauche</source>
        <translation>Tourner l&apos;image de 90° vers la gauche</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="587"/>
        <source>Tourner l&apos;image de 90° vers la droite</source>
        <translation>Tourner l&apos;image de 90° vers la droite</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="769"/>
        <source>Trajectoire</source>
        <translation>Trajectoire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="866"/>
        <source>Chronophotographie</source>
        <translation>Chronophotographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="779"/>
        <source>Chronogramme</source>
        <translation>Chronogramme</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="846"/>
        <source>Montrer les
vecteurs vitesses</source>
        <translation>Montrer les
vecteurs vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1313"/>
        <source>1.0</source>
        <translation>1.0</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1214"/>
        <source>g (N/kg)</source>
        <translation>g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1387"/>
        <source>avec le style </source>
        <translation>avec le style </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1395"/>
        <source>Points seuls</source>
        <translation>Points seuls</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1404"/>
        <source>Points et lignes</source>
        <translation>Points et lignes</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Lignes seules</source>
        <translation>Lignes seules</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Fichier numpy</source>
        <translation>&amp;Fichier numpy</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1720"/>
        <source>Fichier Numpy</source>
        <translation>Fichier Numpy</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>Enregistrer comme image</source>
        <translation>Enregistrer comme image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation>Erreur lors de l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Définir léchelle</source>
        <translation>Définir léchelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>Le nombre d&apos;images par seconde doit être un entier</source>
        <translation>Le nombre d&apos;images par seconde doit être un entier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>merci de recommencer</source>
        <translation>merci de recommencer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation>STOP</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="661"/>
        <source>Efface le point précédent</source>
        <translation>Efface le point précédent</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="699"/>
        <source>Rétablit le point suivant</source>
        <translation>Rétablit le point suivant</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1496"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1566"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation>&amp;Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1588"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1659"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation>&amp;Copier dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1671"/>
        <source>&amp;Défaire</source>
        <translation>&amp;Défaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1689"/>
        <source>Refaire</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1683"/>
        <source>&amp;Refaire</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1137"/>
        <source>Changer d&apos;échelle</source>
        <translation>Changer d&apos;échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1367"/>
        <source>Tracer :</source>
        <translation>Tracer :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1162"/>
        <source>Ajouter les énergies :</source>
        <translation>Ajouter les énergies :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1182"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation>Cinétique (échelle obligatoire)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1189"/>
        <source>Potentielle de pesanteur</source>
        <translation>Potentielle de pesanteur</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1196"/>
        <source>Mécanique</source>
        <translation>Mécanique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation>Suivi automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation>Points à 
étudier :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1169"/>
        <source>Intensité de la pesanteur :</source>
        <translation>Intensité de la pesanteur :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1292"/>
        <source>Données et grandeurs à représenter</source>
        <translation>Données et grandeurs à représenter</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1300"/>
        <source>Masse (kg)</source>
        <translation>Masse (kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1320"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation>Intensité de la pesanteur g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="960"/>
        <source>Changement de référentiel : </source>
        <translation>Changement de référentiel : </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1600"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation>&amp;Enregistrer le projet mecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1645"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation>Ouvrir un projet &amp;mecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>Enregistrer le graphique</source>
        <translation>Enregistrer le graphique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation>Projet Pymecavideo (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation>Enregistrer le projet pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation>Projet pymecavideo (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>fichiers images(*.png)</source>
        <translation>fichiers images (*.png)</translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation>Presser la touche ESC pour sortir</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis Â«Â&#xa0;DÃ©marrer l&apos;acquisitionÂ&#xa0;Â» </source>
        <translation>Choisir le nombre de points puis « Démarrer l&apos;acquisition » </translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="175"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation>Vous pouvez continuer votre acquisition</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="obsolete">Proximité de la souris %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="obsolete">; dernière vidéo %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="obsolete">; videoDir %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation>Proximite de la souris {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation>; derniere video {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation>; videoDir {0}</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="143"/>
        <source>Choisir le ralenti</source>
        <translation type="obsolete">Choisir le ralenti</translation>
    </message>
</context>
</TS>
