<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>MontreFilm</name>
    <message encoding="UTF-8">
        <location filename="../../src/cadreur.py" line="214"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="215"/>
        <source>Ralenti : 1/1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="468"/>
        <source>indéf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1966"/>
        <source>Choisir ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2707"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Nom de fichier non conforme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1470"/>
        <source>point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2183"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2772"/>
        <source>Désolé pas de fichier d&apos;aide pour le langage {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2672"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2738"/>
        <source>Veuillez choisir une image (et définir l&apos;échelle)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2852"/>
        <source>Pymecavideo n&apos;arrive pas à lire l&apos;image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1494"/>
        <source>Pointage Automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1097"/>
        <source>le format de cette vidéo n&apos;est pas pris en charge par pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1270"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Masse de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2352"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>MAUVAISE VALEUR !</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>La valeur rentrée n&apos;est pas compatible avec le calcul</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="518"/>
        <source>Définir l&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="880"/>
        <source>Image n°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="741"/>
        <source>Trajectoires</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="796"/>
        <source>près de la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="806"/>
        <source>partout</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="829"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="836"/>
        <source>px pour 1 m/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1438"/>
        <source>Enregistrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1010"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1043"/>
        <source>Coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1049"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1333"/>
        <source>9.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1280"/>
        <source>Grapheur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1377"/>
        <source>en fonction de </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1513"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1624"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1545"/>
        <source>&amp;Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1574"/>
        <source>avanceimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1579"/>
        <source>reculeimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1615"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1636"/>
        <source>&amp;Exemples ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1650"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1697"/>
        <source>LibreOffice &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1702"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1707"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1712"/>
        <source>&amp;Python (source)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre d&apos;images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="558"/>
        <source>Tourner l&apos;image de 90° vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="587"/>
        <source>Tourner l&apos;image de 90° vers la droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="769"/>
        <source>Trajectoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="866"/>
        <source>Chronophotographie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="779"/>
        <source>Chronogramme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="846"/>
        <source>Montrer les
vecteurs vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1079"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1313"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1214"/>
        <source>g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1387"/>
        <source>avec le style </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1395"/>
        <source>Points seuls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1404"/>
        <source>Points et lignes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Lignes seules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Fichier numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1720"/>
        <source>Fichier Numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>Enregistrer comme image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Définir léchelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>Le nombre d&apos;images par seconde doit être un entier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>merci de recommencer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="661"/>
        <source>Efface le point précédent</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="699"/>
        <source>Rétablit le point suivant</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1496"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1566"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1588"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1659"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1671"/>
        <source>&amp;Défaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1689"/>
        <source>Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1683"/>
        <source>&amp;Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1137"/>
        <source>Changer d&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1367"/>
        <source>Tracer :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1162"/>
        <source>Ajouter les énergies :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1182"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1189"/>
        <source>Potentielle de pesanteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1196"/>
        <source>Mécanique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1169"/>
        <source>Intensité de la pesanteur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1292"/>
        <source>Données et grandeurs à représenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1300"/>
        <source>Masse (kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1320"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="960"/>
        <source>Changement de référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1517"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1600"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1645"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>Enregistrer le graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>fichiers images(*.png)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="175"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
