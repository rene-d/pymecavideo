<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en" sourcelanguage="fr">
<context>
    <name>@default</name>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="preferences.ui" line="13"/>
        <source>Pr&#xe9;f&#xe9;rences de pyMecaVideo</source>
        <translation type="obsolete">Preferences for Pymecavideo</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="19"/>
        <source>&#xc9;chelle des vitesses (px pour 1m/s)</source>
        <translation type="obsolete">Scale for velocities (px by m/s)</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="29"/>
        <source>Vitesses affich&#xe9;es</source>
        <translation type="obsolete">Display velocities</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="39"/>
        <source>Afficheur vid&#xe9;o</source>
        <translation type="obsolete">Video player</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="49"/>
        <source>Niveau de verbosit&#xe9; (d&#xe9;bogage)</source>
        <translation type="obsolete">Verbosity level (debugging)</translation>
    </message>
</context>
<context>
    <name>Label_Echelle</name>
    <message>
        <location filename="echelle.py" line="109"/>
        <source>Choisir le nombre de points puis &quot;D&#xc3;&#xa9;marrer l&apos;acquisition&quot; </source>
        <translation type="obsolete">Choose the number of points then \&quot;Start aquisition\&quot;</translation>
    </message>
</context>
<context>
    <name>MontreFilm</name>
    <message encoding="UTF-8">
        <location filename="../../src/cadreur.py" line="214"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="215"/>
        <source>Ralenti : 1/1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="379"/>
        <source>ind&#xc3;&#xa9;f.</source>
        <translation type="obsolete">undef.</translation>
    </message>
    <message>
        <location filename="__init__.py" line="336"/>
        <source>chemin vers les modules : %s</source>
        <translation type="obsolete">path to the modules: %s</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1041"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="obsolete">time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="__init__.py" line="448"/>
        <source>point N&#xc2;&#xb0; </source>
        <translation type="obsolete">Point N° </translation>
    </message>
    <message>
        <location filename="__init__.py" line="722"/>
        <source>Cliquer sur le point N&#xc2;&#xb0;%d</source>
        <translation type="obsolete">Clic on the point #%d</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Define a scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="obsolete">Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1536"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="obsolete">Please give a valid scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="obsolete">Data will be lost</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Your work has not been saved. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1614"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1650"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Open a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Unvalid filename</translation>
    </message>
    <message>
        <location filename="__init__.py" line="895"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
 Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Your filename contains accented characters or spaces. Please rename it before going further</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1677"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="__init__.py" line="938"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour ce langage %s.</source>
        <translation type="obsolete">Sorry, no help file for this language %s </translation>
    </message>
    <message>
        <location filename="__init__.py" line="1010"/>
        <source>Impossible de lire %s</source>
        <translation type="obsolete">Read %s failed</translation>
    </message>
    <message>
        <location filename="__init__.py" line="1029"/>
        <source>Usage : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</source>
        <translation type="obsolete">Usage : pymecavideo [-f file | --fichier_pymecavideo=file]</translation>
    </message>
    <message>
        <location filename="__init__.py" line="438"/>
        <source>point N&#xc2;&#xb0;</source>
        <translation type="obsolete">point #</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="308"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="obsolete">undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="352"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="obsolete">Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1104"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">point # %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1115"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1262"/>
        <source>Choisir ...</source>
        <translation type="obsolete">Choose...</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation type="obsolete">Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1345"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1376"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">You reached the end of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1627"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1636"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">video files( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov) </translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1711"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="obsolete">Sorry, no help file for the language %1.</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="37"/>
        <source>PyMecaVideo, analyse m&#xe9;canique des vid&#xe9;os</source>
        <translation type="obsolete">Pymecavideo, mechanical analysis of video clips</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="86"/>
        <source>Acquisition des donn&#xe9;es</source>
        <translation type="obsolete">Data acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="189"/>
        <source>Pas de vid&#xe9;os charg&#xe9;es</source>
        <translation type="obsolete">No video loaded</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="144"/>
        <source>Bienvenue sur pymeca vid&#xe9;o, pas d&apos;images charg&#xe9;e</source>
        <translation type="obsolete">Welcome in pymecavideo, no video loaded</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="551"/>
        <source>D&#xe9;finir l&apos;&#xe9;chelle</source>
        <translation type="obsolete">Define the scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="229"/>
        <source>Image n&#xb0;</source>
        <translation type="obsolete">Image #</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="246"/>
        <source>Nombre de points &#xe0; &#xe9;tudier</source>
        <translation type="obsolete">Number of points to study</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="370"/>
        <source>ind&#xe9;f.</source>
        <translation type="obsolete">undef.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation type="unfinished">px/m</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="297"/>
        <source>D&#xe9;marrer l&apos;acquisition</source>
        <translation type="obsolete">Start acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="535"/>
        <source>Tout r&#xe9;initialiser</source>
        <translation type="obsolete">Reinit everything</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="390"/>
        <source>efface le point pr&#xe9;c&#xe9;dent</source>
        <translation type="obsolete">delete previous points</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="455"/>
        <source>r&#xe9;tablit le point suivant</source>
        <translation type="obsolete">restore next points</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="426"/>
        <source>trajectoires et mesures</source>
        <translation type="obsolete">trajectory and measurements</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="526"/>
        <source>Origine du r&#xe9;f&#xe9;rentiel :</source>
        <translation type="obsolete">Origin of the axis:</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="560"/>
        <source>Vid&#xe9;o calcul&#xe9;e</source>
        <translation type="obsolete">Computed video</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="568"/>
        <source>V. normale</source>
        <translation type="obsolete">Normal Vel</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="573"/>
        <source>ralenti /2</source>
        <translation type="obsolete">slower /2</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="578"/>
        <source>ralenti /4</source>
        <translation type="obsolete">slower /4</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="583"/>
        <source>ralenti /8</source>
        <translation type="obsolete">slower /8</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="608"/>
        <source>&#xc9;chelle de vitesses :</source>
        <translation type="obsolete">Scale for velocities:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="836"/>
        <source>px pour 1 m/s</source>
        <translation type="unfinished">px for 1 m/s</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="972"/>
        <source>Coordonn&#xe9;es</source>
        <translation type="obsolete">Coordinates</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1079"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation type="unfinished">Copy data to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="682"/>
        <source>Fichier</source>
        <translation type="obsolete">File</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="753"/>
        <source>Aide</source>
        <translation type="obsolete">Help</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="703"/>
        <source>&#xc9;dition</source>
        <translation type="obsolete">Edit</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2011"/>
        <source>Ouvrir une vid&#xe9;o</source>
        <translation type="obsolete">Open a video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1574"/>
        <source>avanceimage</source>
        <translation type="unfinished">imgforward</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1579"/>
        <source>reculeimage</source>
        <translation type="unfinished">imgbackward</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="738"/>
        <source>Quitter</source>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="743"/>
        <source>Enregistrer les donn&#xe9;es</source>
        <translation type="obsolete">Save data</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="748"/>
        <source>&#xc0; propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1149"/>
        <source>Exemples ...</source>
        <translation type="obsolete">Examples ...</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="763"/>
        <source>Rouvrir un fichier mecavid&#xe9;o</source>
        <translation type="obsolete">Reopen a mecavideo file</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="768"/>
        <source>Pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="773"/>
        <source>copier dans le presse-papier</source>
        <translation type="obsolete">copy to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="280"/>
        <source>Acquisition video</source>
        <translation type="obsolete">Acquire vIdeo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation type="unfinished">Zoom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="209"/>
        <source>Acquisition</source>
        <translation type="obsolete">Acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="384"/>
        <source>D&#xe9;marrer</source>
        <translation type="obsolete">Start</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="418"/>
        <source>efface la s&#xe9;rie pr&#xe9;c&#xe9;dente</source>
        <translation type="obsolete">delete previous series</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="621"/>
        <source>Points &#xe0; 
 &#xe9;tudier:</source>
        <translation type="obsolete">Points to 
study:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="639"/>
        <source>suivi
automatique</source>
        <translation type="obsolete">automatic
tracking</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished">Change the origin</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished">Abscissa to the left
</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="668"/>
        <source>Ordonn&#xe9;es 
vers le bas</source>
        <translation type="obsolete">Ordinate to
the bottom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="741"/>
        <source>Trajectoires</source>
        <translation type="unfinished">Trajectories</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="782"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="obsolete">Show 
velocity 
vectors</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="791"/>
        <source>pr&#xe8;s de
la souris</source>
        <translation type="obsolete">near the
mouse</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="806"/>
        <source>partout</source>
        <translation type="unfinished">everywhere</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="809"/>
        <source>&#xc9;chelle de vitesses</source>
        <translation type="obsolete">Scale of velocities</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="909"/>
        <source>Voir un graphique</source>
        <translation type="obsolete">See a plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1966"/>
        <source>Choisir ...</source>
        <translation>Choose...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="937"/>
        <source>Voir la vid&#xe9;o</source>
        <translation type="obsolete">Watch video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="966"/>
        <source>D&#xe9;finir un autre r&#xe9;f&#xe9;rentiel : </source>
        <translation type="obsolete">Define another reference frame</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="978"/>
        <source>Tableau des dates et des coordonn&#xe9;es</source>
        <translation type="obsolete">Table of timestamps and coordinates</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1058"/>
        <source>Exporter vers ....</source>
        <translation type="obsolete">Export to ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="997"/>
        <source>Oo.o Calc</source>
        <translation type="obsolete">Oo.o Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1073"/>
        <source>Qtiplot</source>
        <translation type="obsolete">Qtiplot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1078"/>
        <source>SciDAVis</source>
        <translation type="obsolete">SciDAVis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1015"/>
        <source>changer d&apos;&#xe9;chelle ?</source>
        <translation type="obsolete">change scale?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1513"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1517"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished">E&amp;xport to ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1624"/>
        <source>&amp;Aide</source>
        <translation type="unfinished">&amp;Help</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1545"/>
        <source>&amp;Edition</source>
        <translation type="unfinished">&amp;Edit</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1102"/>
        <source>&amp;Ouvrir une vid&#xe9;o (Ctrl-O)</source>
        <translation type="obsolete">&amp;Open video (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1120"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="obsolete">Quit (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1128"/>
        <source>Enregistrer les donn&#xe9;es (Ctrl-S)</source>
        <translation type="obsolete">Save data (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1136"/>
        <source>&#xc0; &amp;propos</source>
        <translation type="obsolete">&amp;About</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1141"/>
        <source>Aide (F1)</source>
        <translation type="obsolete">Help (F1)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1154"/>
        <source>Ouvrir un projet &amp;mecavid&#xe9;o</source>
        <translation type="obsolete">Open a &amp;mecavideo project</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1159"/>
        <source>&amp;Pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">&amp;Preferences</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1531"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="obsolete">&amp;Copy to clipboard (Ctrl-C)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1172"/>
        <source>D&#xe9;faire (Ctrl-Z)</source>
        <translation type="obsolete">Undo (Ctrl-Z)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1180"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="obsolete">Redo (Ctrl-Y)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1188"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="obsolete">OpenOffice.org &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1702"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished">Qti&amp;plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1707"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished">Sci&amp;davis</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="314"/>
        <source>Lancer %1
 pour capturer une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation>undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="332"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation>Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1221"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation>time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1245"/>
        <source>point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">point # %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1452"/>
        <source>Veuillez s&#xc3;&#xa9;lectionner un cadre autour de(s) l&apos;objet(s) que vous voulez suivre.
Vous pouvez arr&#xc3;&#xaa;ter &#xc3;&#xa0; tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;ordonn&#xc3;&#xa9;e du point %1</source>
        <translation type="obsolete">Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1496"/>
        <source>Pointage des positions&#xc2;&#xa0;: cliquer sur le point N&#xc2;&#xb0; %1</source>
        <translation type="obsolete">Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation>You reached the end of the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2551"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Define a scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation>Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation> Please give a valid scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation>Data will be lost</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation>Your work has not been saved.
Do you want to save it?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1782"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Nom de fichier non conforme</source>
        <translation>Unvalid filename</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation>The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1862"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1907"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour le langage %1.</source>
        <translation type="obsolete">Sorry, no help file for the language %1.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1798"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation>video files( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="660"/>
        <source>Enregistrer la chronophotographie</source>
        <translation type="obsolete">Save the chronophotography</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation>Image files (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="446"/>
        <source>NON DISPO : {0}</source>
        <translation type="obsolete">UNAVAILABLE: {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1371"/>
        <source>point N&#xb0; {0}</source>
        <translation type="obsolete">Point #{0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de l&apos;abscisse du point {0}</source>
        <translation type="obsolete">Plot of point #{0} abscissa</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1593"/>
        <source>Evolution de l&apos;ordonn&#xe9;e du point {0}</source>
        <translation type="obsolete">Plot of point #{0} ordinate</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de la vitesse du point {0}</source>
        <translation type="obsolete">Plot of point #{0} velocity</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1640"/>
        <source>Pointage des positions&#xa0;: cliquer sur le point N&#xb0; {0}</source>
        <translation type="obsolete">Finding positions: please click on point #{0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2088"/>
        <source>D&#xe9;sol&#xe9; pas de fichier d&apos;aide pour le langage {0}.</source>
        <translation type="obsolete">Sorry, no help file for {0} language.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1976"/>
        <source>fichiers vid&#xe9;os (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">Video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2054"/>
        <source>Veuillez choisir une image (et d&#xe9;finir l&apos;&#xe9;chelle)</source>
        <translation type="obsolete">Please choose an image (and define the scale)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2143"/>
        <source>Pymecavideo n&apos;arrive pas &#xe0; lire l&apos;image</source>
        <translation type="obsolete">Pymecavideo cannot read the image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="882"/>
        <source>Fichier Python cr&#xe9;&#xe9;</source>
        <translation type="obsolete">Python file created</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1494"/>
        <source>Pointage Automatique</source>
        <translation>Auto tracking</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1196"/>
        <source>le format de cette vid&#xe9;o n&apos;est pas pris en charge par pymecavideo</source>
        <translation type="obsolete">The format of this video is not mamnaged by pymecavideo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="468"/>
        <source>indéf.</source>
        <translation type="unfinished">undef.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1097"/>
        <source>le format de cette vidéo n&apos;est pas pris en charge par pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1470"/>
        <source>point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2183"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2707"/>
        <source>Ouvrir une vidéo</source>
        <translation type="unfinished">Open a video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2672"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2738"/>
        <source>Veuillez choisir une image (et définir l&apos;échelle)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2772"/>
        <source>Désolé pas de fichier d&apos;aide pour le langage {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2852"/>
        <source>Pymecavideo n&apos;arrive pas à lire l&apos;image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1270"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Masse de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2352"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>MAUVAISE VALEUR !</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>La valeur rentrée n&apos;est pas compatible avec le calcul</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="518"/>
        <source>Définir l&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="880"/>
        <source>Image n°</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="796"/>
        <source>près de la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="829"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1438"/>
        <source>Enregistrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1010"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1043"/>
        <source>Coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1049"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1333"/>
        <source>9.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1280"/>
        <source>Grapheur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1377"/>
        <source>en fonction de </source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1615"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1636"/>
        <source>&amp;Exemples ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1650"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1697"/>
        <source>LibreOffice &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1712"/>
        <source>&amp;Python (source)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre d&apos;images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="558"/>
        <source>Tourner l&apos;image de 90° vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="587"/>
        <source>Tourner l&apos;image de 90° vers la droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="769"/>
        <source>Trajectoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="866"/>
        <source>Chronophotographie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="779"/>
        <source>Chronogramme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="846"/>
        <source>Montrer les
vecteurs vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1313"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1214"/>
        <source>g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1387"/>
        <source>avec le style </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1395"/>
        <source>Points seuls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1404"/>
        <source>Points et lignes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Lignes seules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Fichier numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1720"/>
        <source>Fichier Numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>Enregistrer comme image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Définir léchelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>Le nombre d&apos;images par seconde doit être un entier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>merci de recommencer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="661"/>
        <source>Efface le point précédent</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="699"/>
        <source>Rétablit le point suivant</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1496"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1566"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1588"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1659"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1671"/>
        <source>&amp;Défaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1689"/>
        <source>Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1683"/>
        <source>&amp;Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1137"/>
        <source>Changer d&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1367"/>
        <source>Tracer :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1162"/>
        <source>Ajouter les énergies :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1182"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1189"/>
        <source>Potentielle de pesanteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1196"/>
        <source>Mécanique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1169"/>
        <source>Intensité de la pesanteur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1292"/>
        <source>Données et grandeurs à représenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1300"/>
        <source>Masse (kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1320"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="960"/>
        <source>Changement de référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1600"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1645"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>Enregistrer le graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>fichiers images(*.png)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation>Type ESC to escape</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation>Choose the number of points to track and then &quot;Start Aquisition&quot; </translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="175"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation>You can keep on with the acquisition</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="obsolete">Near the mouse %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="obsolete">: last video %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="obsolete">: videoDir %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation>Near the mouse {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation>; last video {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation>; videoDir {0}</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="143"/>
        <source>Choisir le ralenti</source>
        <translation type="obsolete">Choose the render rate</translation>
    </message>
</context>
</TS>
