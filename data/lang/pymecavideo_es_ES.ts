<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="es" sourcelanguage="">
<context>
    <name>Dialog</name>
    <message>
        <location filename="." line="144"/>
        <source>Pr&#xe9;f&#xe9;rences de pyMecaVideo</source>
        <translation type="obsolete">Preferencias de pyMecaVideo</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>&#xc9;chelle des vitesses (px pour 1m/s)</source>
        <translation type="obsolete">Escala de velocidades (px para 1m/s)</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Vitesses affich&#xe9;es</source>
        <translation type="obsolete">Velocidades mostradas</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Afficheur vid&#xe9;o</source>
        <translation type="obsolete">Display video</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Niveau de verbosit&#xe9; (d&#xe9;bogage)</source>
        <translation type="obsolete">Nivel de debug</translation>
    </message>
</context>
<context>
    <name>Label_Echelle</name>
    <message>
        <location filename="." line="144"/>
        <source>Choisir le nombre de points puis &quot;D&#xc3;&#xa9;marrer l&apos;acquisition&quot; </source>
        <translation type="obsolete">Escojer el número de puntos y &quot;iniciar la acquisición&quot;</translation>
    </message>
</context>
<context>
    <name>MontreFilm</name>
    <message encoding="UTF-8">
        <location filename="../../src/cadreur.py" line="214"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="215"/>
        <source>Ralenti : 1/1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="379"/>
        <source>ind&#xc3;&#xa9;f.</source>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>chemin vers les modules : %s</source>
        <translation type="obsolete">Carpeta de los Módulos</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1041"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="obsolete">tiempo en segundos, posición en metros</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>point N&#xc2;&#xb0; </source>
        <translation type="obsolete">Point N° </translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Cliquer sur le point N&#xc2;&#xb0;%d</source>
        <translation type="obsolete">Clic en el punto N° :</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Definir la escala</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="obsolete">¿Cuál es el tamaño , en metros de su calibrador en la imagen?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1536"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="obsolete">Usted tiene que dar una escala válida</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="obsolete">Los datos serán perdidos</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Su trabajo no ha sido guardado.
¿Quiere Usted guardarlo ahora?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1614"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Usted alcanzó el principio del video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1650"/>
        <source>Ouvrir une vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Abrir un video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Nombre de archivo no valido</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
 Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">El nombre de su archivo contiene caracteres con acentos o espacios. Por favor retirenlos antes de seguir.</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1677"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Usted debe escojer una imagen y definir la escala</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>D&#xc3;&#xa9;sol&#xc3;&#xa9; pas de fichier d&apos;aide pour ce langage %s.</source>
        <translation type="obsolete">Lo siento, no hay archivo de ayuda para este idioma</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Impossible de lire %s</source>
        <translation type="obsolete">Imposible de leer %s</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Usage : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</source>
        <translation type="obsolete">Uso : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>point N&#xc2;&#xb0;</source>
        <translation type="obsolete">punto N°</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="37"/>
        <source>PyMecaVideo, analyse m&#xe9;canique des vid&#xe9;os</source>
        <translation type="obsolete">PyMecaVideo, análisis mecánica de los videos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="86"/>
        <source>Acquisition des donn&#xe9;es</source>
        <translation type="obsolete">Acquisición de datos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="189"/>
        <source>Pas de vid&#xe9;os charg&#xe9;es</source>
        <translation type="obsolete">No se ha cargado ningún video</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Bienvenue sur pymeca vid&#xe9;o, pas d&apos;images charg&#xe9;e</source>
        <translation type="obsolete">Bienvenidos en pymecavideo, no se ha cargado ningún video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="551"/>
        <source>D&#xe9;finir l&apos;&#xe9;chelle</source>
        <translation type="obsolete">Definir la escala</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="229"/>
        <source>Image n&#xb0;</source>
        <translation type="obsolete">Imagen n°</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Nombre de points &#xe0; &#xe9;tudier</source>
        <translation type="obsolete">Numero de puntos a estudiar</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="370"/>
        <source>ind&#xe9;f.</source>
        <translation type="obsolete">indef.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation type="unfinished">px/m</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>D&#xe9;marrer l&apos;acquisition</source>
        <translation type="obsolete">Iniciar la acquisiciónf</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="535"/>
        <source>Tout r&#xe9;initialiser</source>
        <translation type="obsolete">Reinicializar a todo</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>efface le point pr&#xe9;c&#xe9;dent</source>
        <translation type="obsolete">elimina el punto precedente</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="455"/>
        <source>r&#xe9;tablit le point suivant</source>
        <translation type="obsolete">recupera el punto precedente</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>trajectoires et mesures</source>
        <translation type="obsolete">trayectorias y medidas</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Origine du r&#xe9;f&#xe9;rentiel :</source>
        <translation type="obsolete">Origen del referencial</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Vid&#xe9;o calcul&#xe9;e</source>
        <translation type="obsolete">Video calculado</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>V. normale</source>
        <translation type="obsolete">V. normal</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>ralenti /2</source>
        <translation type="obsolete">velocidad 1/2</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>ralenti /4</source>
        <translation type="obsolete">velocidad 1/4</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>ralenti /8</source>
        <translation type="obsolete">velocidad 1/8</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>&#xc9;chelle de vitesses :</source>
        <translation type="obsolete">Escala de velocidades</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="836"/>
        <source>px pour 1 m/s</source>
        <translation type="unfinished">px para 1 m/s</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="972"/>
        <source>Coordonn&#xe9;es</source>
        <translation type="obsolete">Coordenadas</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1079"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation type="unfinished">Copiar medidas </translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Fichier</source>
        <translation type="obsolete">Archivo</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Aide</source>
        <translation type="obsolete">Ayuda</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>&#xc9;dition</source>
        <translation type="obsolete">Edicion</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2011"/>
        <source>Ouvrir une vid&#xe9;o</source>
        <translation type="obsolete">Abrir un video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1574"/>
        <source>avanceimage</source>
        <translation type="unfinished">adelanta imagen</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1579"/>
        <source>reculeimage</source>
        <translation type="unfinished">retraza imagen</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Quitter</source>
        <translation type="obsolete">Salir</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Enregistrer les donn&#xe9;es</source>
        <translation type="obsolete">Guardar datos</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>&#xc0; propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Rouvrir un fichier mecavid&#xe9;o</source>
        <translation type="obsolete">Reabrir un archivo mecavideo</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>Pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Preferencias</translation>
    </message>
    <message>
        <location filename="." line="144"/>
        <source>copier dans le presse-papier</source>
        <translation type="obsolete">copiar</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1966"/>
        <source>Choisir ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>ind&#xc3;&#xa9;f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en m&#xc3;&#xa8;tre</source>
        <translation type="unfinished">tiempo en segundos, posición en metros</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vid&#xc3;&#xa9;o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2551"/>
        <source>D&#xc3;&#xa9;finir une &#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Definir la escala</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en m&#xc3;&#xa8;tre de votre &#xc3;&#xa9;talon sur l&apos;image ?</source>
        <translation type="unfinished">¿Cuál es el tamaño , en metros de su calibrador en la imagen?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d&apos;indiquer une &#xc3;&#xa9;chelle valable</source>
        <translation type="unfinished">Usted tiene que dar una escala válida</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donn&#xc3;&#xa9;es seront perdues</source>
        <translation type="unfinished">Los datos serán perdidos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n&apos;a pas &#xc3;&#xa9;t&#xc3;&#xa9; sauvegard&#xc3;&#xa9;
Voulez-vous les sauvegarder ?</source>
        <translation type="unfinished">Su trabajo no ha sido guardado.
¿Quiere Usted guardarlo ahora?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1782"/>
        <source>Vous avez atteint le d&#xc3;&#xa9;but de la vid&#xc3;&#xa9;o</source>
        <translation type="obsolete">Usted alcanzó el principio del video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Nom de fichier non conforme</source>
        <translation type="unfinished">Nombre de archivo no valido</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1862"/>
        <source>Veuillez choisir une image et d&#xc3;&#xa9;finir l&apos;&#xc3;&#xa9;chelle</source>
        <translation type="obsolete">Usted debe escojer una imagen y definir la escala</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vid&#xc3;&#xa9;os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caract&#xc3;&#xa8;res accentu&#xc3;&#xa9;s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="882"/>
        <source>Fichier Python cr&#xe9;&#xe9;</source>
        <translation type="obsolete">Creacion del archivo Python</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="468"/>
        <source>indéf.</source>
        <translation type="unfinished">indéf.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1470"/>
        <source>point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2183"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2707"/>
        <source>Ouvrir une vidéo</source>
        <translation type="unfinished">Abrir un video</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2672"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2738"/>
        <source>Veuillez choisir une image (et définir l&apos;échelle)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2772"/>
        <source>Désolé pas de fichier d&apos;aide pour le langage {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2852"/>
        <source>Pymecavideo n&apos;arrive pas à lire l&apos;image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1494"/>
        <source>Pointage Automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1097"/>
        <source>le format de cette vidéo n&apos;est pas pris en charge par pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1270"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Masse de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2335"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2352"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>MAUVAISE VALEUR !</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2650"/>
        <source>La valeur rentrée n&apos;est pas compatible avec le calcul</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="518"/>
        <source>Définir l&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="880"/>
        <source>Image n°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="741"/>
        <source>Trajectoires</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="796"/>
        <source>près de la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="806"/>
        <source>partout</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="829"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1438"/>
        <source>Enregistrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1010"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1043"/>
        <source>Coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1049"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1333"/>
        <source>9.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1280"/>
        <source>Grapheur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1377"/>
        <source>en fonction de </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1513"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1624"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1545"/>
        <source>&amp;Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1615"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1636"/>
        <source>&amp;Exemples ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1650"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1697"/>
        <source>LibreOffice &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1702"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1707"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1712"/>
        <source>&amp;Python (source)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre d&apos;images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="558"/>
        <source>Tourner l&apos;image de 90° vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="587"/>
        <source>Tourner l&apos;image de 90° vers la droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="769"/>
        <source>Trajectoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="866"/>
        <source>Chronophotographie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="779"/>
        <source>Chronogramme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="846"/>
        <source>Montrer les
vecteurs vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1313"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1214"/>
        <source>g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1387"/>
        <source>avec le style </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1395"/>
        <source>Points seuls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1404"/>
        <source>Points et lignes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Lignes seules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Fichier numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1720"/>
        <source>Fichier Numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="671"/>
        <source>Enregistrer comme image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Définir léchelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>Le nombre d&apos;images par seconde doit être un entier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2797"/>
        <source>merci de recommencer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="661"/>
        <source>Efface le point précédent</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="699"/>
        <source>Rétablit le point suivant</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo.py" line="1496"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1566"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1588"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1659"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1671"/>
        <source>&amp;Défaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1689"/>
        <source>Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1683"/>
        <source>&amp;Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1137"/>
        <source>Changer d&apos;échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1367"/>
        <source>Tracer :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1162"/>
        <source>Ajouter les énergies :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1182"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1189"/>
        <source>Potentielle de pesanteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1196"/>
        <source>Mécanique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1169"/>
        <source>Intensité de la pesanteur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1292"/>
        <source>Données et grandeurs à représenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1300"/>
        <source>Masse (kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1320"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="960"/>
        <source>Changement de référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1517"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1600"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1645"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2126"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>Enregistrer le graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1108"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1418"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2119"/>
        <source>fichiers images(*.png)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis &#xc2;&#xab;&#xc2;&#xa0;D&#xc3;&#xa9;marrer l&apos;acquisition&#xc2;&#xa0;&#xc2;&#xbb; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="175"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
